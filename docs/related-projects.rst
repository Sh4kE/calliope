Related projects
================

Open projects
-------------

`Beets <https://beets.io>`_ is an open source commandline-based music
organisation tool.

`OpenWhyd <https://openwhyd.org/>`_ is an open source, platform-independent
website and app for collecting music and creating playlists.

`Smarter Playlists <https://github.com/plamere/SmarterPlaylists>`_ by Paul Lamere
is an online service for rules-based generation of music playlists.

`Spiff Radio <https://www.spiff-radio.org/>`_ is a platform-independent website
for browsing and sharing playlists. It's built with the open source
`WP SoundSystem <https://github.com/gordielachance/wp-soundsystem/>`_
plugin for Wordpress, and the proprietary
`SoundSystem API <https://github.com/gordielachance/wp-soundsystem/wiki/SoundSystem-API>`_.

`Tracker <https://gnome.pages.gitlab.gnome.org/tracker/>`_ is a open source
system service that provides simple indexing and searching of music for the
GNOME desktop.

Proprietary services
--------------------

`The Hype Machine <https://hypem.com/>`_ scrapes the web to create playlists
of recommended music.

`Radiooooo <https://radiooooo.com/>`_ is a paid service that lets you generate
playlists by country and decade.

`Roon <https://roonlabs.com/>`_ provides proprietary software to organise and
stream music around your home, with a cloud-based recommendations engine.

`Spotify <https://spotify.com>`_ funds a lot of [research into playlist
generation](https://research.atspotify.com/).

`Swarm.fm <http://app.swarm.fm/>`_ connects with Spotify to produce
recommendations of new releases by artists you listen to, and other things.

Research
--------

A selection of relevant papers. There are many more.

  * `"Scaling Up Music Playlist Generation"
    <https://www.researchgate.net/publication/2907224_Scaling_Up_Music_Playlist_Generation>`_,
    *Aucouturier and Pachet*

      * Uses `adaptive search
        <http://pauillac.inria.fr/~diaz/adaptive/manual/index.html>`_ to select
        music according to user-defined constraints.

  * `"Fast Generation of Optimal Music Playlists using Local Search"
    <https://www.researchgate.net/publication/220723500_Fast_Generation_of_Optimal_Music_Playlists_using_Local_Search>`_,
    *Pauws, Verhaegh, and Vossen*, 2006

      * Uses `simulated annealing
        <https://en.wikipedia.org/wiki/Simulated_annealing>`_ to select music
        according to user-defined constraints.

  * `"Automatic Playlist Generation from Self-Organizing Music Map"
    <https://www.researchgate.net/publication/275681650_Automatic_Playlist_Generation_from_Self-Organizing_Music_Map#citations>`_,
    *Hartono and Yoshitake*, 2013

      * Uses a `self-organising map
        <https://en.wikipedia.org/wiki/Self-organizing_map>`_ based on musical
        features

  * `"Automated Generation of Music Playlists: Survey and Experiments"
    <https://web-ainf.aau.at/pub/jannach/files/Journal_ACM_2015.pdf>`_, *Bonnin
    and Jannach*, 2015
