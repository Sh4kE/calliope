Examples
========

The examples are meant to be simple and clear. All of them make use of
`shell pipelines <https://effective-shell.com/docs/part-2-core-skills/7-thinking-in-pipelines/>`_
to combine different Calliope commands.  Take them and play with them, modify
them, break them, and improve them.

The :doc:`commandline API <reference-cli>` used by these examples is the
primary way to interact with Calliope, but there is also a :mod:`Python API
<calliope>`.

.. toctree::
   :maxdepth: 2

   examples/simple
   examples/collectors
   examples/listen-history
   examples/similar-tracks
   examples/misc
