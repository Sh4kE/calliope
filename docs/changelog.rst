Changelog
=========

Unreleased
----------
 * spotify: Export and Import tracks, albums or artists from/into the current
   users' spotify library
   By :user:`lackhove`.
   :mr:`168`

3.1
---

 * spotify: Fix breakage with Python 3.7.
   By :user:`lackhove`.
   :mr:`164`
 * spotify: Fix a broken testcase
   By :user:`lackhove`.
   :mr:`165`

3.0
---

 * spotify: Use the much improved resolver introduced in 2.0.0 and extend
   playlist import action to use spotify IDs and URIs and update existing
   playlists.
   By :user:`lackhove`.
   :mr:`155`.
 * spotify: Remove the ``--user`` flag, it did not do what it claimed to do.
   :mr:`158`.
 * Document how to get Spotify API keys.
   :mr:`161`.
 * lastfm-history: Move progress bar to stderr.
   :mr:`160`.
 * Other small fixes and documentation improvements.

2.0
---

 * Replace --debug with --verbosity CLI option.
   Thanks to :user:`lackhove`.
   :mr:`149`.
 * Skip tests if module requirements aren't installed.
   Thanks to :user:`lackhove`.
   :mr:`151`.
 * Update CI image with some follow-up fixes.
 * musicbrainz: Add a much improved resolver.
   Thanks to :user:`lackhove`.
   :mr:`148`.
 * spotify: Small improvements to resolver, add playlist import.
   :mr:`150`.
 * youtube: Fix mass playlist export
   :bug:`85`.
