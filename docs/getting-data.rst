Getting data
============

Everything new starts from something old.  Calliope provides tools to pull data
from various sources, which you can use to generate recommendations.

.. toctree::
   :maxdepth: 2

   getting-data/local
   getting-data/online
   getting-data/api-keys
