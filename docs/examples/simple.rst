Simple examples
===============

Random playlist
---------------

The simplest option is to shuffle your local music collection and then
select songs until the desired duration is reached. See
`examples/random-playlist.sh <https://gitlab.com/samthursfield/calliope/-/blob/master/examples/random-playlist.sh>`_:

.. literalinclude:: ../../examples/simple/random.sh
    :start-after: set -e
    :language: bash
