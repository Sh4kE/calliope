.. calliope documentation master file, created by
   sphinx-quickstart on Sun Nov 25 01:26:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Calliope documentation
======================
Calliope is a toolkit for working with playlists of music.

The goal is to enable research and experimentation in open source music
recommendation, by providing a set of standard interfaces and operations
to online and local music services.

In order words, a way to mash up data from Spotify, Musicbrainz, Last.fm
and your local music collection!

Calliope is distributed on PyPI as `calliope-music <https://pypi.org/project/calliope-music/>`_
and can be installed like this:

.. code:: bash

    pip install calliope-music

For more details, see the `INSTALL.md file <https://gitlab.com/samthursfield/calliope/-/blob/master/INSTALL.md>`_.

This manual documents some of the things that are possible using the
Calliope playlist toolkit.

.. toctree::
   :maxdepth: 3

   basics
   getting-data
   making-playlists
   listening-to-music
   examples
   related-projects
   format
   reference-cli
   autoapi/index
   changelog
